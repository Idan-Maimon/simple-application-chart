{{- define "mycharts.labels" }}
    generator: 'helm'
    blackops/managed: 'true'
    blackops/tenant: 'horus'
    date: '{{ now | htmlDate }}'
    blackops/monitored: 'true'
{{- end }}
